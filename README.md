I wanted to compare languages and GUI toolkits, so I picked a fairly straight forward game
and will write it using a variety of lanuages and GUI toolkits.

At time of writing, I've written the Kolin/JavaFX version, and started on the C#/WinForms version.
I chose the write both without the aid of an IDE or GUI designer.

Languages
=========

Kotlin
------

Pros

* Concise
* * No semi colons
* * No duplication of types on field declarations with assignments.
* * Constructors can automatically assign arguments to fields without extra code.
* Null-safe most of the time.
* val keyword encourages immutable states (leading to fewer bugs).
* Many nice language features, such as "apply"
* Lambdas are very nice to use. Much better than Java's.
* No need to place functions inside classes as static methods.
* No restriction on what can be placed in a single file. I don't like having a separate file
  for tiny classes, such as simple enums.
* I18N is easy to set up (using Java's ResourceBundle class).


Cons

* Complicated to compile from the command line - Gradle (or other build tool) is needed
* Doesn't create an executable; requires a JVM to be installed  (and be on the path) to run it.
* Documentation lacking - the API documentation are writen from a Java standpoint.
  So you need to know how to translate from Java to Kotlin. I had trouble using Callback from Kotlin

C#
--

Pros

* Easy to compile, even without a build tool.
* Creates a simple executable (unlike jvm based languages)

Cons

* Lots of boiler plate.
* * Semi-colons
* * duplication of type names
* * A constructor has to manually assign arguments to fields
* * Lack of "apply" means code is littered with repeated field names
    (especially when setting up gui controls).
    Note, there is an "apply" like construct, but only when creating NEW instances.

* Cannot declare and initialise fields in one place.
  Initialisation must be separate from the declaration.
* No "apply" or "with" constructs. You can mimic them a little, but not close enough to be useful.
* Other people tend to use proprietry build tools (i.e. Visual Studio), therefore doing the
  "open" thing is going against the grain.
* Everything is a class. No good old functions.
* Weird case conventions. Classes and Properties both start with capital letters.
  e.g. Foo.Bar, is Foo a class name or a Property name? Could be either!



GUI Toolkits
============

JavaFX
------

Pros

* Looks good.
* Clear separation between logic and presentation (See minesweeper.css)
* Easy to lay out components in a flexible manner (they automatically move when the window is resized).
* The SplitMenuButton is very nice for the "New Game" button and menu.
* Any controls can be added to Toolbars, unlike WinForms ToolStrip.
* Use of Properties make updating the gui really nice. Cuts out lots of spagetti programming.

Cons

* Spinners default to non-editable. Weird!
* Spinners throws exceptions when the editor field is blank
* Spinners silently clamp the value into the range set in the constructor
* Need a command line bodge to display correctly on linux with hi DPI display (export GDK-SCALE=2)
* JavaFX became a "module" after version 8, which adds complexity to the build process.
* The need for Platform.runLater is annoying - you must keep track of which methods will be run in
  a different thread to the GUI. (e.g. the Timer's thread). This can get cumbersome in larger
  applications.
* Potential for the GUI to creep into code where it doesn't belong. It is very tempting to use
  Property objects within business data structures (so that updating the data will automatically
  update the GUI).
  However, that would then add a strong dependency of the GUI into the business code. Nasty!

WinForms
--------

Pros

* Easy to place components using pixel locations (not something I want to do though!)

I really tried to think of more Pros, but I'm at a loss.

Cons

* ToolStrips are limited - they can only contain component specially designed for ToolStrips.
  AFAIK, there is no way to have a toolstrip with items on the left, center and right
  (which is what I wanted here).
* Hard to lay out components in a flexible manner. Maybe this is from my lack of experience with 
  WinForms, but despite numerous attempts, and lots of google searches, I'm yet to find a "good"
  way to lay out copmonents in a flexible manner without hard coding the positioning.
* Getting it to respect my system's default font size was tricky on the main form,
  and impossible on the MessageBox.
* Presentation is mixed together with logic. There is dreadful compared to JavaFX's styling via css.
* Setting up the controls is long and cumbersome. It is no wonder that most people use the
  GUI designer, as the alternative is painful!
* Adding child controls uses AddRange, which is badly named, and takes an ARRAY. Why not a "params"
  parameter (aka varargs in Java), and why not call it AddAll?
* No equavalent of JavaFX's properties, so when updating the counter, and the revealed spaces,
  we need to manually update the GUI components. This isn't a problem in this case, but more complex
  application would become more spagetti like.



