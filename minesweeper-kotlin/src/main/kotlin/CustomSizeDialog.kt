import java.util.Random

import javafx.application.*
import javafx.event.*
import javafx.scene.*
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.scene.input.*
import javafx.scene.image.*
import javafx.geometry.*
import javafx.util.*

import javafx.stage.*

private fun X( suffix : String ) = i18n( "customSize.$suffix" )

class CustomSizeDialog : Dialog<SkillLevel?>() {

    
    val across = Spinner<Int>(6,100, 10).apply {
        isEditable = true    
    }
    
    val down = Spinner<Int>(6,100, 10).apply {
        isEditable = true
    }
    
    val mineCount = Spinner<Int>(1, 500, 10).apply {   
        isEditable = true
    }
    
    val sizeGrid = GridPane().apply {
        styleClass.add( "fields" )
        
        add( Label( X("across") ), 0,0 )
        add( across, 1, 0 )
        
        add( Label( X("down") ), 0,1 )
        add( down, 1, 1 )
    }
    
    
    val titledSize = TitledPane( X("size"), sizeGrid ).apply {
        isCollapsible = false
    }
    
    val minesGrid = GridPane().apply {
        styleClass.add( "fields" )

        add( Label( X("numberOfMines") ), 0,0 )
        add( mineCount, 1, 0 )
    }

    val vBox = VBox().apply {
        styleClass.add("fields")
        children.addAll( titledSize, minesGrid )
    }

    val error = Label( " " )
    
    val borderPane = BorderPane().apply {
        center = vBox
        bottom = error
    }
    
    init {
        dialogPane.styleClass.add( "form" )
        
        title = X("customSize")
        headerText = X("chooseSize")
        dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
        dialogPane.content = borderPane
        
        resultConverter = Callback { buttonType ->
            if (buttonType == ButtonType.OK) {
                SkillLevel( X("custom"), across.value, down.value, mineCount.value )
            } else {
                null
            }
        }

        (dialogPane.lookupButton(ButtonType.OK) as? Button)
            ?.addEventFilter(ActionEvent.ACTION, EventHandler { event ->
                // Grrr, the Spinners throw exceptions when their values are invalid
                // Such as blank.
                // Grrr, if the value is too small, or too large, it is SILENTLY adjusted.
                // I have no solution for that yet.
                try {
                    if ( mineCount.value > across.value * down.value / 2 ) {
                        event.consume()
                        error.text = X("error.tooManyMines" )
                    }
                } catch (e : Exception) {
                    event.consume()
                    error.text = X("error.allRequired")
                }
            }
        )
            
        val css = MineSweeper::class.java.getResource("minesweeper.css").toExternalForm()
        dialogPane.scene.stylesheets.add(css)
    }
    
}
