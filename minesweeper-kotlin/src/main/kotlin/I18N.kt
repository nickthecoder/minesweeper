import java.util.*

var resourceBundle = ResourceBundle.getBundle( "minesweeper" )
var fallbackResourceBundle = ResourceBundle.getBundle( "minesweeper", Locale.ENGLISH )

fun i18n( key : String ) : String {
    return try {
        resourceBundle.getString( key )
    } catch (e : MissingResourceException) {
        println( "WARNING Failed to find I18N resource : $key" )
        try {
            fallbackResourceBundle.getString( key )
        } catch (e : MissingResourceException) {
            // If all else fails, return the key itself.
            key
        }
    }
}

/**
 * Replaces fields in the form : ${n} with the corresponding item from [inserts].
 * 
 * Note this is ZERO based, so ${1} would be replaced with the second item in the [inserts].
 */
fun i18n( key : String, vararg inserts : Any ) : String {

    var result = i18n( key )
    inserts.forEachIndexed{ index, insert ->
        result = result.replace( "\${${index}}", insert.toString() )
    }
    return result
    
}
