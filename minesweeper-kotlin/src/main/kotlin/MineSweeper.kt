import java.util.*

import javafx.application.*
import javafx.event.*
import javafx.scene.*
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.scene.input.*
import javafx.scene.image.*
import javafx.geometry.*
import javafx.util.*
import javafx.beans.property.*

import javafx.stage.*


/**
 * The main entry point for the program.
 * 
 * Launches the [MineSweeper] [Application].
 * The only other "classes" are the enums : [ImageType] and [GridState],
 * the data-only "structure" [GridItem], and the [CustomSizeDialog].
 */
fun main( vararg args : String ) {
    Application.launch( MineSweeper::class.java, *args )
}

private const val GRID_WIDTH = 32.0
private const val GRID_HEIGHT = 32.0

/**
 * The names of the images that can appear in each square.
 * The first three, are the three states that the user can toggle through.
 * [N0] to [N8] are the revealed number neighbouring mines.
 * The last three are only used when the game is lost.
 * 
 * [EXPLOSION] is used then a square thought to be empty actually contained a mine.
 * [MINE] is used to show the location of all undiscovered mines.
 * [NO_MINE] is used to show flagged locations which did NOT contain a mine.
 */
enum class ImageType {
    BLANK, UNKNOWN, FLAGGED, N0, N1, N2, N3, N4, N5, N6, N7, N8, EXPLOSION, MINE, NO_MINE
}

/**
 * Each square in the grid can be cycled through three states [BLANK], [UNKNOWN] and [FLAGGED]
 * using the right mouse button.
 * 
 * When the player thinks the square is "safe", they can reveal it, using the left mouse button.
 */
enum class GridState {
    BLANK, UNKNOWN, FLAGGED, REVEALED
}

data class GridItem(
    var isMine : Boolean = false,
    var state : GridState = GridState.BLANK,
    var imageView : ImageView? = null
)

data class SkillLevel(
    val name : String,
    val across : Int,
    val down : Int,
    val mineCount : Int
)

private fun X( suffix : String ) = i18n( "mineSweeper.$suffix" )
private fun X( suffix : String, vararg inserts : Any ) = i18n( "mineSweeper.$suffix", *inserts )

val beginner = SkillLevel( X("skill.beginner" ), 9, 9, 10 )
val intermediate = SkillLevel( X("skill.intermediate"), 16, 16, 40 )
val expert = SkillLevel( X("skill.expert"), 30, 16, 99 )


class MineSweeper() : Application() {

    private val images = HashMap<ImageType,Image?>()
    
    // Size of the game grid
    private var across = 0
    private var down = 0

    private var isPlaying = false

    /**
     * The number of sqaures that must be revealed to win the game.
     */
    private var yetToRevealCount = 0

    /**
     * Number of squares flagged.
     * We use a JavaFX property, so that the label in the toolbar is automatically
     * updated when the [flaggedCount] is changed.
     */
    private val flaggedCountProperty = SimpleIntegerProperty(0)    
    private var flaggedCount : Int
        get() = flaggedCountProperty.get()
        set(v) { flaggedCountProperty.set(v) }
    
    /**
     * The 2D array of data for the grid.
     */
    private lateinit var grid : Array<Array<GridItem>>
    
    /**
     * Set at the beginning of [newGame].
     */
    private var currentSkillLevel = beginner
        
    private var random = Random()

    private var timer : Timer? = null
    
    private var startTime = 0L
    
    
    // GUI fields
    
    private val newGameButton = SplitMenuButton().apply {
        text = X( "newGame" )
        onAction = EventHandler { newGame() }
        
        items.addAll(
            createSkillMenuItem( beginner ),
            createSkillMenuItem( intermediate ),
            createSkillMenuItem( expert ),
            SeparatorMenuItem(),
            MenuItem( X("customSize") ).apply {
                onAction = EventHandler{ onCustomSize() }
            }
        )
    }
    
    private val flaggedCountLabel = Label().apply {
        textProperty().bind( flaggedCountProperty.asString() )
        styleClass.add( "flagged" )
        tooltip = Tooltip( X("flagsPlaced") )
    }
    
    private val elapsedLabel = Label( "0" ).apply {
        styleClass.add( "elapsed" )
        tooltip = Tooltip( X("timeElapsed") )  
    }

    val toolBar = ToolBar().apply {
        items.addAll(
            flaggedCountLabel,
            Pane().apply { HBox.setHgrow(this, Priority.ALWAYS) },
            newGameButton,
            Pane().apply { HBox.setHgrow(this, Priority.ALWAYS) },
            elapsedLabel
        )
    }
    
    lateinit var container : Pane
        
    val whole = BorderPane().apply {
        top = toolBar
        center = Label( X("welcome") ).apply {
            styleClass.add( "welcome" )
        }
    }
    
    override fun start(stage: Stage) {
        
        // Load all the images
        for ( item in ImageType.values()) {
            images[item] = loadImage( "${item.name.toLowerCase()}.png" )
        }

        with( stage ) {
            title = X("mineSweeper")
            scene = Scene( whole )
        }
        val css = MineSweeper::class.java.getResource("minesweeper.css").toExternalForm()
        stage.scene.stylesheets.add(css)

        stage.show()
    }
  
    private fun createSkillMenuItem( skillLevel : SkillLevel ) : MenuItem {
        return MenuItem( skillLevel.name ).apply {
            onAction = EventHandler {
                newGame( skillLevel )
            }
        }
    }
    
    private fun onCustomSize() {
        val reply = CustomSizeDialog().showAndWait()
        if (reply.isPresent()) {
            newGame( reply.get() )
        }
    }
    
    private fun newGame() {
        newGame( currentSkillLevel )
    }
    
    
    private fun newGame( skillLevel : SkillLevel ) {
        currentSkillLevel = skillLevel
        
        startTime = Date().time
        val everySecondTask = object : TimerTask() {
            override fun run() {
                Platform.runLater {
                    elapsedLabel.text = "${(Date().time - startTime) / 1000 }"   
                }
            }
        }
        timer?.cancel()
        timer = Timer("everySecond", true).apply {
            schedule(everySecondTask, 1000, 1000) // Every 1 second
        }
        
        this.across = skillLevel.across
        this.down = skillLevel.down
        this.container = Pane().apply {
            maxWidth = across * GRID_WIDTH
            maxHeight = down * GRID_HEIGHT
            onMouseClicked = EventHandler{ event -> onClicked( event ) }
            BorderPane.setMargin(this, Insets(12.0,12.0,12.0,12.0))
        }
        this.whole.center = container

        // Initially, all grid positions are shown as "blank", and have no mines.
        this.grid = Array(across) { Array( down ) {
            GridItem()
        }}
        
        // Ooh look, a nested function declaration ;-)
        fun addMine() : Boolean {
            val x = random.nextInt( across )
            val y = random.nextInt( down )
            if ( grid[x][y].isMine ) return false
            grid[x][y].isMine = true
            return true
        }
        
        // Add all of the mines. If mines > across * down, then this will loop forever.
        for ( m in 1..skillLevel.mineCount ) {
            do {
                //println( "Laying mine #$m" )
            } while( !addMine() )
        }
        for (x in 0 until across) {
            for (y in 0 until down) {
                setAt( x, y, GridState.BLANK, ImageType.BLANK )
            }
        }

        whole.scene.window.sizeToScene()
        yetToRevealCount = across * down - skillLevel.mineCount
        flaggedCount = 0
        isPlaying = true
    }
    
    
    private fun endGame( won : Boolean ) {
        isPlaying = false
        timer?.cancel()
        timer = null
        if (won) {
            Alert(Alert.AlertType.INFORMATION, X("cleared", elapsedLabel.text)).apply {
                title = X("wellDone")
                headerText = title
                showAndWait()
            }
            // TODO highscore?
        } else {
        
            // Show all the un-flagged mines, as well as incorrectly flagged squares.
            for ( y in 0 until currentSkillLevel.down ) {
                for ( x in 0 until currentSkillLevel.across ) {
                    val item = grid[x][y]
                    if (item.state != GridState.REVEALED) {
                        
                        if (item.state == GridState.FLAGGED && ! item.isMine ) {
                            setAt(x, y, GridState.REVEALED, ImageType.NO_MINE)
                        } else if ( item.state != GridState.FLAGGED && item.isMine ) {
                            setAt(x, y, GridState.REVEALED, ImageType.MINE )
                        }
                        
                    } 
                }
            }
        }
        
    }
    
    fun setAt( x : Int, y : Int, state : GridState, imageType : ImageType ) {
        if (onGrid(x,y)) {
            val item = grid[x][y]
            
            if (item.state == GridState.FLAGGED) {
                flaggedCount--
            }
            if (state == GridState.FLAGGED) {
                flaggedCount ++
            }
            
            item.state = state
            
            if (item.imageView != null) {
                container.children.remove( item.imageView )
            }
            val iv = ImageView( images[imageType] )
            iv.x = x * GRID_WIDTH
            iv.y = y * GRID_HEIGHT 
            item.imageView = iv
            container.children.add( iv )
            iv.relocate(iv.x, iv.y)
            //println( "Set $x, $y to $imageType" )
        
        }
    }
    
    /**
     * Is [x],[y] within the bounds of the current mine field [grid]
     */
    fun onGrid( x : Int, y : Int ) = x >= 0 && y >=0 && x < across && y < down
    
    /**
     * Called when either mouse button is clicked.
     * The left button will call [reveal], and the right button will call [toggle].
     */
    private fun onClicked( event : MouseEvent ) {
        if ( isPlaying ) {
            val x = (event.x / GRID_WIDTH).toInt()
            val y = (event.y / GRID_HEIGHT).toInt()
            if ( onGrid(x,y) ) {            
                if ( event.button == MouseButton.PRIMARY ) {
                    reveal( x, y )
                } else if ( event.button == MouseButton.SECONDARY ) {
                    toggle( x, y )
                }
            }
            
            if (isPlaying && yetToRevealCount == 0 && flaggedCount == currentSkillLevel.mineCount) {
                endGame(true)
            }

        }
    }
    
    /**
     * Reveal the squares that they player believes to be empty.
     * If [GridItem.state] is [GridState.REVEALED], then we look at the number of neighbours,
     * make sure that this many flags have been placed around, and if so, reveal all un-revealed
     * neighbours.
     * 
     * If on the other hand [GridItem.state] is [GrudState.BLANK], then we reveal THIS square.
     * 
     * When we reveal a square, if it has no neighbouring mines, then we reveal all neighbours too.
     * 
     * If we reveal a square which contains a mine ([GridItem.isMine] == true), then the game is lost.
     * 
     * NOTE, This method does NOT check for the winning condition. See [onClicked].
     */
    private fun reveal( x : Int, y : Int ) {
        if (onGrid(x,y)) {
            val item = grid[x][y]
            if ( item.state == GridState.BLANK ) {
                if ( item.isMine ) {
                    setAt(x,y, GridState.REVEALED, ImageType.EXPLOSION )
                    endGame( false )
                } else {
                    val neighbourCount = mineCount( x, y )
                    setAt(x, y, GridState.REVEALED, ImageType.valueOf( "N${neighbourCount}" ) )
                    yetToRevealCount --
                    if ( neighbourCount == 0 ) {
                        for ( tx in x - 1 .. x + 1 ) {
                            for ( ty in y - 1 .. y + 1 ) {
                                reveal( tx, ty )
                            }
                        }
                    }
                }
            } else if ( item.state == GridState.REVEALED ) {
                // If the number of flags == mine count, then reveal all blank neighbours.
                // This is different to the Windows version of mine sweeper.
                // This way only needs two types of mouse click, Windows needs 3 types
                val mineCount = mineCount( x, y )
                val flagCount = flagCount( x, y )
                if ( mineCount == flagCount ) {
                    for ( tx in x - 1 .. x + 1 ) {
                        for ( ty in y - 1 .. y + 1 ) {
                            if (onGrid(tx, ty)) {
                                val other = grid[tx][ty]
                                if (other.state == GridState.BLANK) {
                                    reveal( tx, ty )
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    /**
     * Called when the right button is clicked on un-revealed squares.
     * 
     * Cycles the [GridItem.state] through the three states
     * [GridState.BLANK], [GridState.FLAGGED] and [GridState.UNKNOWN].
     */
    private fun toggle( x : Int, y : Int ) {
        if (onGrid(x,y)) {
            val item = grid[x][y]
            when (item.state) {
                GridState.BLANK -> setAt( x, y, GridState.FLAGGED, ImageType.FLAGGED )
                GridState.FLAGGED -> setAt( x, y, GridState.UNKNOWN, ImageType.UNKNOWN )
                GridState.UNKNOWN -> setAt( x, y, GridState.BLANK, ImageType.BLANK )
                else -> {}
            }
        }
    }
    
    
    
    /**
     * Counts the number of mines surrounding this grid location.
     * Also counts THIS grid location (but this method isn't called when the grid location has a mine)
     */
    fun mineCount( x : Int, y : Int ) : Int {
        var total = 0
        for (i in x-1 .. x+1) {
            for (j in y-1 .. y+1) {
                if ( onGrid(i,j) && grid[i][j].isMine ) {
                    total ++
                }
            }
        }
        return total
    }
    
    fun flagCount( x : Int, y : Int ) : Int {
        var total = 0
        for (i in x-1 .. x+1) {
            for (j in y-1 .. y+1) {
                if ( onGrid(i,j) && grid[i][j].state == GridState.FLAGGED ) {
                    total ++
                }
            }
        }
        return total
    }
}


private fun loadImage( name : String ) : Image? {
    val imageStream = MineSweeper::class.java.getResourceAsStream(name)
    return if (imageStream == null) {
        println( "WARNING. Failed to load image $name" )
        null 
    } else {
        Image(imageStream)
    }
}

