# Minesweeper

To play the game, you must first install
[Tickle](https://github.com/nickthecoder/tickle).

Start Tickle, then click "Play", and choose the file : "Minesweeper.tickle".
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).



Powered by [Tickle](https://github.com/nickthecoder/tickle) and [LWJGL](https://www.lwjgl.org/).
