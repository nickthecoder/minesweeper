/*
Shows the game's elapsed time in seconds.

*/
class Timer : AbstractRole {

    // We are using the standard Java Date class. It will be used to calculate the number of seconds elapsed.
    val startTime = Date().time

    // So that we don't update the text every frame, keep track of the previous value,
    // -1 ensures that the value is updated immediately, as the new value will be 0.
    var oldValue = -1

    override fun tick() {
        if (Play.instance.isPlaying) {
            // Integer division (throw away the fractional part), leaving the number of seconds since the
            // scene started.
            val seconds = (Date().time - startTime) ~/ 1000 
            if (seconds != oldValue) {
                oldValue = seconds
                actor.textAppearance.text = "$seconds"
            }
        }
    }
}
